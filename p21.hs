divisors :: Int -> [Int]
divisors x =
    [c | c <- [1,2..(x - 1)], x `mod` c == 0]

y x = sum $ divisors x

amicables :: Int -> [Int]
amicables n = 
    [c | c <- [2,3..n], c == d (d c), c /= (d c)]
    where
        d x = sum $ divisors x

p21 = sum $ amicables 10000

main = print p21
