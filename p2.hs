fibs :: [Integer]
fibs = 1:1:(zipWith (+) fibs (tail fibs))

p2 x = 
    sum [c | c <- takeWhile (< x) fibs, even c]
