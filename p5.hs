divisible :: Int -> [Int] -> Bool
divisible x (y:ys) 
    | x `mod` y == 0 = divisible x ys
    | otherwise = False
divisible x _ = True

p5 :: Int
p5 = head [x | x <- [2520,2520*2..], divisible x [1,2..20]]
