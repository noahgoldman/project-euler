#include <stdio.h>
#include <stdlib.h>

int sequence(long n, int count)
{
        if (n == 1) {
                return count;
        }
        else if (n % 2 == 0) {
                count++;
                return sequence(n/2, count);
        }
        else {
                count++;
                return sequence(3 * n + 1, count);
        }
}

void main()
{
        long int i, top;
        int max = 0;
        for (i = 1; i < 1000000; i++) {
                int counts = sequence(i, 0);
                printf("%d\n", i);
                if (counts > max) {
                        max = counts;
                        top = i;
                }
        }

        printf("%d", top);
}
