import Data.Char

series n = n^n : series (n+1)

seriessum :: Int -> Integer
seriessum n = sum $ take n $ series 1

last10 :: Integer -> [Int]
last10 x = drop ((length digits) - 10) $ digits
    where
        getDigits n = map digitToInt $ show n
        digits = getDigits x

p25 = last10 $ seriessum 1000

main = print p25
