#include <stdio.h>
#include <math.h>

int triangle(int n)
{
	if (n == 1) {
		return 1;
	}
	else {
		return (n + triangle(n-1));
	}
}

void main()
{
	int i = 1, j, count = 0;
	while (count < 500) {
		count = 0;
		int tri = triangle(i);
		printf("%d\n", tri);
		for (j = 1; j <= sqrt(tri); j++) {
			if (tri % j == 0) {
				printf("	%d - %d\n", j, count);
				count += 2;
			}
		}
		i++;
	}

	printf("%d", triangle(i-1));
}
