primes :: Int -> [Int]
primes n = 2 : sieve [3,5..n] where
    sieve [] = []
    sieve (p:xs) = p : sieve (minus xs [p, p+2*p..n])

minus (x:xs) (y:ys) = case (compare x y) of
    LT -> x : minus xs (y:ys)
    EQ -> minus xs ys
    GT -> minus (x:xs) ys
minus xs _ = xs

p3 n = 
    maximum [x | x <- primes n, 600851475143 `mod` x == 0]
