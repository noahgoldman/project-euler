#include <iostream>
using namespace std;

int chain(int num, int count)
{
	if (num == 1) {
		return count;
	}
	else if (num % 2 == 0) {
		count++;	
		chain(num/2, count);
	}
	else {
		count++;
		chain(num*3+1, count);
	}
}

int main()
{
	int a = chain(1000, 0);
	cout << a;
}
