fibs :: [Integer]
fibs = 0:1:(zipWith (+) fibs (tail fibs))

p25 :: Int
p25 = length (takeWhile (<10^999) fibs)

main = print p25
