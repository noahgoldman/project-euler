digits :: Int -> [Int]
digits 0 = []
digits n = n `mod` 10 : digits (n `div` 10)

palindrome n = (digits n == reverse (digits n))

p4 :: Int
p4 = maximum [x*y | x <- [1,2..999], y <- [1,2..999], palindrome $ x*y]
