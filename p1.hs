p1 x = sum [ c | c <- [1..x], c `mod` 5 == 0 || c `mod` 3 == 0 ] 

main = p1 1000
